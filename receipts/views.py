from django.shortcuts import redirect, render
from receipts.forms import CreateAccount, CreateCategory, CreateReceipt
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def show_receipts(request):
    item = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": item}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()
            return redirect("home")

    else:
        form = CreateReceipt()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    item = ExpenseCategory.objects.filter(owner=request.user)
    context = {"category_list": item}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    item = Account.objects.filter(owner=request.user)
    context = {"account_list": item}
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.owner = request.user
            receipts.save()
            return redirect("category_list")

    else:
        form = CreateCategory()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.owner = request.user
            receipts.save()
            return redirect("account_list")

    else:
        form = CreateAccount()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
